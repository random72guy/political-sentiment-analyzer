# Political Sentiment Analyzer

## Overview

In the upcoming presidential election, Americans will have to choose a presidential candidate. Moreover, major parties have yet to nominate their candidates. However, in our increasingly fast-paced and interconnected world, it is difficult to track how our candidates respond to current events. This problem is compounded by bias in and distrust of conventional media sources. 

What if the electorate could easily visualize candidates’ reactions to current events and news coverage? What if they could go directly to the source, to the words of the candidates themselves? This project aims to deliver that capability via a web application combining the Twitter API with sentiment analysis and modern visualization technology. 

## Data Sources

This project requires data sources. The following are recommended.

* [All the News 2.0 : 2.7 million news articles – Components](https://components.one/datasets/all-the-news-2-news-articles-dataset/)
* [Trump Tweets | Kaggle](https://www.kaggle.com/austinreese/trump-tweets#)

Input files must be placed in the dir: `ui/src/assets`. See `news.csv` and `tweets.csv` and `tweet.ts` and `news-heading.ts` in `ui/src/types` for the required formats.

Input files can be of type `Tweet` or `NewsHeadline`. Ensure the format of the tweets is CSV, with headers matching the interfaces in the `ui/src/types` directory. 


## Running the Project

Prerequisites: 

* Docker & Docker Compose
* NodeJS 12.14.1

Run sentiment service via:

```
docker-compose up
```

Run UI via:

```
cd ui
# npm ci, if you haven't already.
npm start
```

The Web Service is not used at this time. It exists for potential integration with Twitter APIs at a later date. To start it up, use:

```
cd ws
# npm ci, if you haven't already.
npm start
```

## Developing

See each project sub-folder for more information. 


### UI

The UI is built using the following technologies: 

* [Angular](https://angular.io/) - model/view/view-controller framework.
* [Angular Material](https://material.angular.io/components/categories) - UI components.
* [Angular Flex Layout](https://github.com/angular/flex-layout) - Flexible layout manager.
  * [API Overview](https://github.com/angular/flex-layout/wiki/Declarative-API-Overview)
* [Lodash](https://lodash.com/docs) - Data transformation, functional programming, utility methods.
* [NGX Charts](https://github.com/swimlane/ngx-charts) - Charting libraries.
  * [Demos](https://swimlane.github.io/ngx-charts/)
  * [Documentation](https://swimlane.gitbook.io/ngx-charts/)
* [ngx-datatable](https://swimlane.gitbook.io/ngx-datatable/)

### WS

The Web Service (WS) is built using the following technologies: 

* [Express](https://expressjs.com/)
* [Twitter for Node.js](https://www.npmjs.com/package/twitter)
* [Twitter API](https://developer.twitter.com/en/docs)

Start for development with: 

```
cd ws
npm run dev
```

The server will automatically rebuild as changes are made.
 
### Sentiment Service

The sentiment analysis service uses the [DeepAI Sentiment Analysis Docker container](https://hub.docker.com/r/deepaiorg/sentiment-analysis/). More info on the service and its API can be found [here](https://deepai.org/machine-learning-model/sentiment-analysis).

To test it, try something like: 

```
docker-compose up
curl -F 'text=What a wonderful day!' http://127.0.0.1:5000/
```


# Potential Improvements

## Alternative Sentiment Analyzers

The DeepAI Sentiment Analyzer only supports a spectrum of positive/negative. Other open-source options might be desirable. (Note: compatibility with Docker is also a plus.) Here is a short list of potential alternatives:

* [Stanford NLP Deeply Moving](https://nlp.stanford.edu/sentiment/), [more info](https://nlp.stanford.edu/sentiment/code.html). Included in [CoreNLP](https://stanfordnlp.github.io/CoreNLP/), which has a [web service](https://stanfordnlp.github.io/CoreNLP/corenlp-server.html) and an [official Docker container](https://hub.docker.com/r/frnkenstien/corenlp) (documented [here](https://stanfordnlp.github.io/CoreNLP/corenlp-server.html#docker)). However, it's unclear a) whether the API exposes the semantic analysis, and b) what format the output is in. Unfortunately the [demo site](http://nlp.stanford.edu:8080/sentiment/rntnDemo.html) is down.
* [Awesome Open Source Sentiment Analysis Listing](https://awesomeopensource.com/projects/sentiment-analysis) - A ranked list of open-source sentiment analysis projects.
* [NLP.js](https://github.com/axa-group/nlp.js) provides many capabilities for multiple language. Its [sentiment analysis](https://github.com/axa-group/nlp.js/blob/master/docs/v3/sentiment-analysis.md) seems to return a numeric positive/negative score. It supports polarity and subjectivity [sentiment analysis](https://www.clips.uantwerpen.be/pages/pattern-en#sentiment).
* [Pattern](https://github.com/clips/pattern) is a robust Python NLP library. 
  * [Documentation](https://www.clips.uantwerpen.be/pages/pattern).
  * Also supports [mood and modality](https://www.clips.uantwerpen.be/pages/pattern-en#modality) classification, which could allow visual analysis of user accounts' tone.
* [Sentiment](https://github.com/thisandagain/sentiment) focuses on sentiment analysis, and provides details on how it ranks tokens within phrases (which could help displaying a visual sentence breakdown). Written in JS, it may be able to be run on the browser, and could definitely be exposed with [Express](https://expressjs.com/) endpoints. 


## Text to Emoji

When scanning / comparing many tweets, distilling text to visual form is useful. Words can be converted to emoji using [Text to Emoji](https://github.com/niladridutt/Text_to_Emoji).

Related: 

* [Emoji Emoticon](https://github.com/words/emoji-emotion) - A list of emoticons rated by sentiment.
* [DeepMoji](https://github.com/nolis-llc/DeepMoji-docker) ([Demo](https://deepmoji.mit.edu/)) - Converts words to emoji using deep learning.
