import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './submodules/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AnalyzerComponent } from './components/analyzer/analyzer.component';
import { FormsModule } from '@angular/forms';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { HttpClientModule } from '@angular/common/http';
import { TimelineComponent } from './components/timeline/timeline.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SelectedItemComponent } from './components/selected-item/selected-item.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import {NgxLocalStorageModule} from 'ngx-localstorage';
import { TweetDetailComponent } from './components/tweet-detail/tweet-detail.component';
import { NewsHeadlineDetailComponent } from './components/news-headline-detail/news-headline-detail.component';
import { ElmDetailComponent } from './components/elm-detail/elm-detail.component';
import { SentimentToImagesPipe } from './pipes/sentiment-to-images.pipe';


@NgModule({
  declarations: [
    AppComponent,
    AnalyzerComponent,
    TimelineComponent,
    SelectedItemComponent,
    TweetDetailComponent,
    NewsHeadlineDetailComponent,
    ElmDetailComponent,
    SentimentToImagesPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule, 
    FormsModule, 
    MatMomentDateModule,
    HttpClientModule,
    NgxDatatableModule,
    NgxChartsModule,
    NgxLocalStorageModule.forRoot({
      prefix: 'political-sentiment-analyzer',
      allowNull: false,
    }),
  ],
  providers: [
    SentimentToImagesPipe,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
