import { Component, OnInit } from '@angular/core';
import { TwitterService } from 'src/app/services/twitter.service';
import { NewsService } from 'src/app/services/news.service';
import { Tweet } from 'src/types/tweet';
import { NewsHeadline } from 'src/types/news-headline';
import * as _ from 'lodash';
import { TableColumn, ColumnMode, SelectionType } from '@swimlane/ngx-datatable';
import { SelectedItemTypes } from 'src/types/selected-item-types';
import { DataFilters } from 'src/types/data-filter';
import { SelectedItemService } from 'src/app/services/selected-item.service';
import { SentimentAnalysisService } from 'src/app/services/sentiment-analysis.service';
import { DataFilterService } from 'src/app/services/data-filter.service';

@Component({
  selector: 'app-analyzer',
  templateUrl: './analyzer.component.html',
  styleUrls: ['./analyzer.component.scss']
})
export class AnalyzerComponent implements OnInit {

  public SelectionType = SelectionType;
  public selectedTabIndex = 0;

  public tables: {
    label: String,
    itemsKey: 'filteredTweets'|'filteredHeadlines',
    itemType: SelectedItemTypes,
    columns: TableColumn[],
  }[] = [
      {
        label: 'Tweets',
        itemType: SelectedItemTypes.TWEET,
        itemsKey: 'filteredTweets',
        columns: [
          { prop: 'date' },
          {
            prop: 'content',
            width: 350,
          },
          { prop: 'mentions' },
          { prop: 'hashtags' },
          { prop: 'retweets' },
          { prop: 'favorites' },
          { prop: 'sentimentImages'}
        ],
      },
      {
        label: 'News Headlines',
        itemType: SelectedItemTypes.NEWS_HEADLINE,
        itemsKey: 'filteredHeadlines',
        columns: [
          { prop: 'date' },
          {
            prop: 'title',
            width: 350,
          },
          { prop: 'publication' },
          { prop: 'section' },
          { prop: 'author' },
          { prop: 'url' },
          { prop: 'sentimentImages' },
        ]
      }
    ];


  public selectedItems: (Tweet | NewsHeadline)[] = [];

  constructor(
    private twitterService: TwitterService,
    public newsService: NewsService,
    private selectedItemService: SelectedItemService,
    public dataFilterService: DataFilterService,
    public sentimentAnalysisService: SentimentAnalysisService,
  ) {
  }

  ngOnInit(): void {
    this.twitterService.loadFromCsv('assets/data/tweets.csv').then(tweets => {
      this.dataFilterService.dataFilters.allTweets = tweets;
      this.dataFilterService.dataFilters.apply();
    });
    this.newsService.loadFromCsv('assets/data/news.csv').then(headlines => {
      this.dataFilterService.dataFilters.allHeadlines = headlines;
      this.dataFilterService.dataFilters.apply();
    });

    this.dataFilterService.dataFilters.$filterApplied.subscribe(()=>{
      Promise.all([
        this.sentimentAnalysisService.processAll(this.dataFilterService.dataFilters.filteredTweets),
        this.sentimentAnalysisService.processAll(this.dataFilterService.dataFilters.filteredHeadlines),
      ]);
    })
  }

  public onTableRowClick(type: SelectedItemTypes) {
    this.selectedItemService.setSelectedItem(this.selectedItems[0], type);
  }

  public onSelectedTabChange() {
    this.dataFilterService.dataFilters.apply(); // Regenerate chart. (Bugfix for an issue where it disappears and doesn't re-render.)
  }

}

