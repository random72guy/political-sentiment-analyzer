import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElmDetailComponent } from './elm-detail.component';

describe('ElmDetailComponent', () => {
  let component: ElmDetailComponent;
  let fixture: ComponentFixture<ElmDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElmDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElmDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
