import { Component, OnInit, Input } from '@angular/core';
import { NewsHeadline } from 'src/types/news-headline';
import { Tweet } from 'src/types/tweet';

@Component({
  selector: 'app-elm-detail',
  templateUrl: './elm-detail.component.html',
  styleUrls: ['./elm-detail.component.scss']
})
export class ElmDetailComponent implements OnInit {

  @Input() typeTitle: 'Tweet' | 'Headline';
  @Input() date: Date;
  @Input() content: string;
  @Input() detailProps: string[];
  @Input() elm: Tweet | NewsHeadline;
  @Input() url: string;

  constructor() { }

  ngOnInit(): void {
  }

}
