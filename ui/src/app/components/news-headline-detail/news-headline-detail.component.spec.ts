import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsHeadlineDetailComponent } from './news-headline-detail.component';

describe('NewsHeadlineDetailComponent', () => {
  let component: NewsHeadlineDetailComponent;
  let fixture: ComponentFixture<NewsHeadlineDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsHeadlineDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsHeadlineDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
