import { Component, OnInit, Input } from '@angular/core';
import { NewsHeadline } from 'src/types/news-headline';

@Component({
  selector: 'app-news-headline-detail',
  templateUrl: './news-headline-detail.component.html',
  styleUrls: ['./news-headline-detail.component.scss']
})
export class NewsHeadlineDetailComponent implements OnInit {

  @Input() newsHeadline: NewsHeadline;
  public details: (keyof NewsHeadline)[] = [
    'publication',
    'author',
    'section',
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
