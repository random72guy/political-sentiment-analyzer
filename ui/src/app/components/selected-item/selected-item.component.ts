import { Component, OnInit, Input } from '@angular/core';
import { NewsHeadline } from 'src/types/news-headline';
import { Tweet } from 'src/types/tweet';
import { SelectedItemTypes } from 'src/types/selected-item-types';
import { SelectedItemService } from 'src/app/services/selected-item.service';
import { SentimentAnalysisService } from 'src/app/services/sentiment-analysis.service';
import { Sentiment } from 'src/types/sentiment';

@Component({
  selector: 'app-selected-item',
  templateUrl: './selected-item.component.html',
  styleUrls: ['./selected-item.component.scss']
})
export class SelectedItemComponent implements OnInit {

  public SelectedItemTypes = SelectedItemTypes;
  public headlineDetails: (keyof NewsHeadline)[] = [
    'publication',
    'author',
    'section',
    'sentiments',
  ];
  public tweetDetails: (keyof Tweet)[] = [
    'retweets',
    'favorites',
    'mentions',
    'hashtags',
    'sentiments',
  ];

  constructor(
    public selectedItemService: SelectedItemService,
  ) { }

  ngOnInit(): void {
    this.selectedItemService.$selectedItem.subscribe( () => {
      const text = this.selectedItemService.selectedItemType == SelectedItemTypes.TWEET ? (this.selectedItemService.selectedItem as Tweet).content : (this.selectedItemService.selectedItem as NewsHeadline).title;
    });
  }

}
