import { Component, OnInit } from '@angular/core';
import { SelectedItemService } from 'src/app/services/selected-item.service';
import { BubbleChartDataItem, BubbleChartSeries, BubbleChartMultiSeries } from '@swimlane/ngx-charts'
import * as _ from 'lodash';
import { Sentiment } from 'src/types/sentiment';
import { SelectedItemTypes } from 'src/types/selected-item-types';
import { DataFilterService } from 'src/app/services/data-filter.service';
const moment = require('moment');

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss']
})
export class TimelineComponent implements OnInit {

  public chartData: BubbleChartMultiSeries;
  public xAxisTicks: Date[];

  constructor(
    private selectedItemService: SelectedItemService,
    private dataFiltersService: DataFilterService,
  ) { }

  ngOnInit(): void {
    this.dataFiltersService.dataFilters.$filterApplied.subscribe(() => this.updateChartData());
  }

  public formatXAxisTick(val: Date) {
    return moment(val).format('M/D ha');
  }

  private updateChartData() {
    console.log('updateChartData()');

    const tweetSeries: BubbleChartSeries = {
      name: 'Tweets',
      series: _.map(this.dataFiltersService.dataFilters.filteredTweets, tweet => {
        const elm: BubbleChartDataItem = {
          x: moment(tweet.date).startOf('hour').toDate(),
          y: this.combineSentiments(tweet.sentiments),
          r: 1,
          name: tweet.content,
          extra: tweet,
        };
        return elm;
      }),
    };

    const newsSeries: BubbleChartSeries[] = _.chain(this.dataFiltersService.dataFilters.filteredHeadlines)
      .groupBy(headline => headline.publication)
      .map((headlines, publication) => {
        const series: BubbleChartSeries = {
          name: publication,
          series: _.map(headlines, headline => {
            const dataItem: BubbleChartDataItem = {
              x: moment(headline.date).startOf('hour').toDate(),
              y: this.combineSentiments(headline.sentiments),
              r: 1,
              name: headline.publication,
              extra: headline,
            }
            return dataItem;
          }),
        }
        return series;
      })
      .value();

    this.chartData = [tweetSeries, ...newsSeries];
    console.log('chartData', this.chartData);
  }

  public onSelect(chartElm): void {
    const elm = chartElm.extra;
    const type = elm.content ? SelectedItemTypes.TWEET : SelectedItemTypes.NEWS_HEADLINE;
    this.selectedItemService.setSelectedItem(elm, type);
  }

  private sentimentToNumberMap: { [sentiment in Sentiment]: number } = {
    "Very Negative": -2,
    "Very Positive": 2,
    Negative: -1,
    Neutral: 0,
    Positive: 1,
  }

  private combineSentiments(sentiments: Sentiment[]): number {
    if (_.isEmpty(sentiments)) return 0;
    return _.meanBy(sentiments, sentiment => this.sentimentToNumberMap[sentiment]);
  }

}
