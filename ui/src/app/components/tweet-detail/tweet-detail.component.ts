import { Component, OnInit, Input } from '@angular/core';
import { Tweet } from 'src/types/tweet';

@Component({
  selector: 'app-tweet-detail',
  templateUrl: './tweet-detail.component.html',
  styleUrls: ['./tweet-detail.component.scss']
})
export class TweetDetailComponent implements OnInit {

  @Input() tweet: Tweet;
  public details: (keyof Tweet)[] = [
    'retweets',
    'favorites',
    'mentions',
    'hashtags',
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
