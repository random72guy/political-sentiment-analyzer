import { Pipe, PipeTransform } from '@angular/core';
import { Sentiment } from 'src/types/sentiment';
import * as _ from 'lodash';

@Pipe({
  name: 'sentimentToImages'
})
export class SentimentToImagesPipe implements PipeTransform {

  private map: {[s in Sentiment]: string} = {
    "Very Negative": '😣',
    "Very Positive": "😁",
    Negative: "🙁",
    Neutral: "🙂",
    Positive: "😐",
  };

  transform(sentiments: Sentiment[]): string[] {
    return _.map(sentiments, s => this.map[s]);
  }

}
