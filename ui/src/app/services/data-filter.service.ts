import { Injectable } from '@angular/core';
import { DataFilters } from 'src/types/data-filter';

@Injectable({
  providedIn: 'root'
})
export class DataFilterService {

  public dataFilters = new DataFilters();

  constructor() { }
}
