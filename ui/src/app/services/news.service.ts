import { Injectable } from '@angular/core';
import { NewsHeadline } from 'src/types/news-headline';
import { parse } from 'papaparse';
import * as _ from 'lodash';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  public headlines: NewsHeadline[];
  public validPublications = [
    'Business Insider',
    'Washington Post',
    'CNBC',
    'The New York Times',
    'CNN',
  ];

  constructor(
    private http: HttpClient,
  ) { }

  public loadFromCsv(filePath: string): Promise<NewsHeadline[]> {
    return this.http.get(filePath, { responseType: 'text' }).toPromise().then(fileContents => {
      const parseRes = parse(fileContents, {
        header: true,
      });
      const headlines = _.chain(parseRes.data)
        .filter(headline => _.includes(this.validPublications, headline.publication))
        .map(headline => {
          return _.assign({}, headline, {
            id: parseInt(headline.id),
            date: new Date(headline.date),
          });
        })
        .value();

      const groupedHeadlines = _.groupBy(headlines, headline => headline.publication);
      console.log('headlines', headlines, groupedHeadlines);

      this.headlines = headlines;
      return this.headlines;
    });
  }
}
