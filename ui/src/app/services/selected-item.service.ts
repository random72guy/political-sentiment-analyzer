import { Injectable } from '@angular/core';
import { SelectedItemTypes } from 'src/types/selected-item-types';
import { Tweet } from 'src/types/tweet';
import { NewsHeadline } from 'src/types/news-headline';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SelectedItemService {

  private _selectedItem: Tweet | NewsHeadline;
  private _selectedItemType: SelectedItemTypes;
  public $selectedItem = new Subject<void>();

  constructor() { }

  public setSelectedItem(item: Tweet | NewsHeadline, type: SelectedItemTypes) {
    this._selectedItem = item;
    this._selectedItemType = type;
    this.$selectedItem.next();
    console.log('Set selected item!', this);
  }

  get selectedItem() {
    return this._selectedItem;
  }
  get selectedItemType() {
    return this._selectedItemType;
  }
}
