import { Injectable } from '@angular/core';
import { Sentiment } from 'src/types/sentiment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NewsHeadline } from 'src/types/news-headline';
import { Tweet } from 'src/types/tweet';
import * as _ from 'lodash';
import { DataFilterService } from './data-filter.service';
import { LocalStorageService } from 'ngx-localstorage';
import { SentimentToImagesPipe } from '../pipes/sentiment-to-images.pipe';

@Injectable({
  providedIn: 'root'
})
export class SentimentAnalysisService {

  public processing: number = 0;
  private localStorageKey = 'sentiment';

  constructor(
    private http: HttpClient,
    private dataFilterService: DataFilterService,
    private localStorage: LocalStorageService,
    private sentimentToImages: SentimentToImagesPipe,
  ) { }

  public process(text: string): Promise<Sentiment[]> {

    const fd = new FormData();
    fd.append('text', text);

    return this.http.post<Sentiment[]>('/sentimentAnalysis/', fd, {
      headers: new HttpHeaders({ "Accept": "application/json" })
    }).toPromise();
  }

  public processAll(elms: (Tweet | NewsHeadline)[]) {

    this.processing++;

    // Apply cached sentiment where necessary.
    _.filter(elms, elm => _.isEmpty(elm.sentiments))
      .forEach(elm => {
        elm.sentiments = this.getElmSentimentFromCache(elm);
        elm.sentimentImages = this.sentimentToImages.transform(elm.sentiments);
      });

    // Ask server to determine sentiment.
    const promises = _.filter(elms, elm => _.isEmpty(elm.sentiments))
      .map(elm =>
        this.process(_.get(elm, 'content') + _.get(elm, 'title')).then(res => {
          elm.sentiments = res;
          elm.sentimentImages = this.sentimentToImages.transform(elm.sentiments);
      })
      );

    // After all requests resolve:
    Promise.all(promises).then(res => {
      _.forEach(elms, elm => this.cacheElmSentiment(elm));
      this.processing--;
      // Update timeline, but prevent infinite loop.
      if (_.filter(elms, elm => _.isEmpty(elm.sentiments)).length) this.dataFilterService.dataFilters.apply();
    });

  }

  private cacheElmSentiment(elm: Tweet | NewsHeadline) {
    this.localStorage.set(`${this.localStorageKey}_${elm.id}`, elm.sentiments);
  }

  private getElmSentimentFromCache(elm: Tweet | NewsHeadline) {
    return this.localStorage.get(`${this.localStorageKey}_${elm.id}`) as Sentiment[];
  }
}

interface SentimentCache {
  [headlineId: number]: Sentiment[];
}