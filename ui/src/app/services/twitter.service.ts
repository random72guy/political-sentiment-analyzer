import { Injectable } from '@angular/core';
import { Tweet } from 'src/types/tweet';
import { parse } from 'papaparse';
import * as _ from 'lodash';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TwitterService {

  public tweets: Tweet[];

  constructor(
    private http: HttpClient,
  ) { }

  public loadFromCsv(filePath: string): Promise<Tweet[]> {

    return this.http.get(filePath, { responseType: 'text' }).toPromise().then(fileContents => {
      const parseRes = parse(fileContents, {
        header: true,
      });
      this.tweets = _.map(parseRes.data, tweet => {
        if (tweet.mentions) tweet.mentions = tweet.mentions.split(',');
        return tweet;
      });

      console.log('tweets', this.tweets, parseRes);
      return this.tweets;
    });

  }
}
