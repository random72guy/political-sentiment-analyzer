import { Tweet } from './tweet';
import { NewsHeadline } from './news-headline';
const moment = require('moment');
import * as _ from 'lodash';
import { Subject } from 'rxjs';

export class DataFilters {
    public text: string;
    public dateRange: {
      startDate: Date,
      endDate: Date;
    }
    public publications: string[];

    public $filterApplied = new Subject<void>();
  
    public allTweets: Tweet[] = [];
    public filteredTweets: Tweet[] = [];
  
    public allHeadlines: NewsHeadline[] = [];
    public filteredHeadlines: NewsHeadline[] = [];
  
    constructor() {
      this.init();
    }
  
    public init() {
      this.filteredHeadlines = _.clone(this.allHeadlines);
      this.filteredTweets = _.clone(this.allTweets);
      this.publications = [
        'Business Insider',
      ];
  
      this.text = '';
      this.dateRange = {
        startDate: new moment('03-26-2020').toDate(),
        endDate: new moment('04-02-2020').toDate(),
      };
    }
  
    public clear() {
      this.text = '';
      this.dateRange = {
        startDate: null,
        endDate: null,
      };
    }
  
    public apply = _.debounce(this._apply, 300);
    private _apply() {

      let filteredTweets: Tweet[];
      let filteredHeadlines: NewsHeadline[];

      // Filter tweets and headlines by dates.
      filteredTweets = _.filter(this.allTweets, tweet =>
        moment(tweet.date).isBetween(this.dateRange.startDate, this.dateRange.endDate, null, '[]') as boolean);
      filteredHeadlines = _.filter(this.allHeadlines, headline =>
        _.includes(this.publications, headline.publication)
        && moment(headline.date).isBetween(this.dateRange.startDate, this.dateRange.endDate, null, '[]') as boolean
      );
  
      // Filter elms by text.
      if (this.text) filteredTweets = _.filter(filteredTweets, tweet =>
        _.includes(JSON.stringify(tweet).toLowerCase(), this.text.toLowerCase()));
      if (this.text) filteredHeadlines = _.filter(filteredHeadlines, headline =>
          _.includes(JSON.stringify(headline).toLowerCase(), this.text.toLowerCase()));


      this.filteredTweets = filteredTweets;
      this.filteredHeadlines = filteredHeadlines;

      this.$filterApplied.next();
    }
  
  }