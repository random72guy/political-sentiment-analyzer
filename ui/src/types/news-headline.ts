import { Sentimental } from './sentimental';

export interface NewsHeadline extends Sentimental {
    id: number;
    date: Date,
    author: string;
    title: string;
    url: string;
    section: string;
    publication: string;
}