export enum Sentiment {
    VERY_POSITIVE = 'Very Positive', 
    POSITIVE = 'Positive', 
    NEUTRAL = 'Neutral', 
    NEGATIVE = 'Negative', 
    VERY_NEGATIVE = 'Very Negative', 
}