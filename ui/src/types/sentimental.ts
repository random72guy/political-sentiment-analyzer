import { Sentiment } from './sentiment';

export interface Sentimental {
    sentiments?: Sentiment[],
    sentimentImages?: string[],
    emoticonSummary?: string[],
}