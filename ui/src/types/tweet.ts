import { Sentimental } from './sentimental';

export interface Tweet extends Sentimental {
    id: number;
    link: string;
    content: string;
    date: Date;
    retweets: number;
    favorites: number;
    mentions: string[];
    hashtags: string;
}
